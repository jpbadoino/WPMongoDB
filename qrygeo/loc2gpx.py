# This script generates a GPX file for the location
# provided as a parameter by referring to the MongoDB
# collection PCRJ_Locais.
import sys
import bson
from gpx import GPX
from pymongo import MongoClient

# Parameter = location name
if len(sys.argv) < 2:
    print("The \"location name\" parameter is required.")
    print("Syntax: " + sys.argv[0] + " location_name")
    sys.exit()

location_name = str(sys.argv[1])
outfile = location_name.replace(" ", "_").lower() + ".gpx"

# GPX class
gpx = GPX()
gpx.gpxbeg(location_name, \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# Location data
regx = bson.regex.Regex(location_name)
location = db.PCRJ_Locais.find_one({"properties.Nome" : regx })
if location:
    print(location)
    name = location['properties']['Nome']
    desc = location['properties']['Descricao']
    lat = location['geometry']['coordinates'][1]
    lon = location['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
else:
    print("Location not found")
    sys.exit()

gpx.gpxend()
gpx.write(outfile)

#  WordPress OpenStreetMap Plugin
gpx.wposmap(outfile)