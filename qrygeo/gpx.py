"""
GPX

Class to generate a GPX file
"""
import sys
import html
import ntpath
import datetime

class GPX:
    
    def __init__(self):
        self.data = []
        self.minlat = 90
        self.maxlat = -90
        self.minlon = 180
        self.maxlon = -180
        self.outfile = (ntpath.basename(sys.argv[0]).split("."))[0] + ".gpx"

    def gpxbeg(self, name, description):
        self.data.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
        self.data.append("<gpx version=\"1.1\" " + \
          "creator=\"Reficio - http://reficio.cc\" " + \
          "xmlns=\"http://www.topografix.com/GPX/1/1\" " + \
          "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " + \
          "xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 " + \
          "http://www.topografix.com/GPX/1/1/gpx.xsd\">\n")
        self.data.append("\t<metadata>\n")
        self.data.append("\t\t<name>" + name + "</name>\n")
        self.data.append("\t\t<desc>" + description + "</desc>\n")
        self.data.append("\t\t<author>\n")
        self.data.append("\t\t\t<name>Halley Pacheco de Oliveira</name>\n")
        self.data.append("\t\t\t<email id=\"reficio\" domain=\"reficio.cc\" />\n")
        self.data.append("\t\t\t<link href=\"http://reficio.cc/\">\n")
        self.data.append("\t\t\t\t<text>Reficio</text>\n")
        self.data.append("\t\t\t</link>\n")
        self.data.append("\t\t</author>\n")
        self.data.append("\t</metadata>\n")

    def gpxend(self):
        self.data.append("</gpx>")

    def wpt(self, name, desc, lat, lon):
        self.data.append("\t<wpt lat=\"" + str(lat) + \
                         "\" lon=\"" + str(lon) + "\">\n")
        self.data.append("\t\t<name>" + html.escape(name) + "</name>\n")
        self.data.append("\t\t<desc>" + html.escape(desc) + "</desc>\n")
        self.data.append("\t</wpt>\n")

    def trkbeg(self, name):
        self.data.append("\t<trk>\n")
        self.data.append("\t\t<name>" + name + "</name>\n")

    def trkend(self):
        self.data.append("\t</trk>\n")

    def trksegbeg(self):
        self.data.append("\t\t<trkseg>\n")

    def trksegend(self):
        self.data.append("\t\t</trkseg>\n")

    def trkpt(self, lat, lon):
        self.data.append("\t\t\t<trkpt lat=\"" + str(lat) + \
                         "\" lon=\"" + str(lon) + "\"></trkpt>\n")

    def write(self, outfile=None):
        if outfile == None:
            outfile = self.outfile

        f = open(outfile, "w")
        for line in self.data:
            f.write(line)

        f.close()

    def minmaxlatlon(self, lat, lon):
        self.maxlat = max(lat, self.maxlat)
        self.minlat = min(lat, self.minlat)
        self.maxlon = max(lon, self.maxlon)
        self.minlon = min(lon, self.minlon)

    #  WordPress OpenStreetMap Plugin
    def wposmap(self, outfile=None):
        if outfile == None:
            outfile = self.outfile

        medlat = (self.minlat + self.maxlat)/2
        medlon = (self.minlon + self.maxlon)/2
        d = datetime.date.today()
        s = "[osm_map_v3 map_center=\"" + \
            str(medlat) + "," + str(medlon) + \
            "\" zoom=\"14\" width=\"100%\" height=\"480\" " + \
            "map_border=\"thin solid grey\" " + \
            "file_list=\"../../../../wp-content/uploads/" + \
            '{:04d}'.format(d.year) + "/" + '{:02d}'.format(d.month) + \
            "/" + outfile + "\" file_color_list=\"blue\"]"
        print(s)
