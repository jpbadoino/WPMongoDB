# This script genaretes a gpx file for
#      "denomination=>catholic"
# in Open Street Map osmpoints collection.
from gpx import GPX
from pymongo import MongoClient
import bson

# GPX class
gpx = GPX()
gpx.gpxbeg("OSM denomination=catholic", \
           "Open Street Map")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# For all denomination=>catholic
regx = bson.regex.Regex('"denomination"=>"catholic"')
for property in db.osmpoints.find({"properties.other_tags" : regx }):
    tags = property['properties']['other_tags']
    name = property['properties']['name']
    osm_id = property['properties']['osm_id']
    lat = property['geometry']['coordinates'][1]
    lon = property['geometry']['coordinates'][0]
    gpx.wpt(name, osm_id, lat, lon)
    gpx.minmaxlatlon(lat, lon)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()