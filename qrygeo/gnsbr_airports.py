# Airports in Brazil
# This script genaretes a gpx file for
# airports in gnsbr collection.
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Airports in Brazil", \
           "NGA GEOnet Names Server (GNS)")

# Connect to reficio database on MongoDB server
conn = MongoClient('192.168.0.7', 27017)
db = conn.reficio

# For all airports in Brazil
for airp in db.gnsbr.find({'features.properties.DSG' : "AIRP"}):
    print(airp)
    name = airp['features'][0]['properties']['FULL_NAME_RO']
    desc = airp['features'][0]['properties']['GENERIC']
    lat = airp['features'][0]['geometry']['coordinates'][1]
    lon = airp['features'][0]['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()