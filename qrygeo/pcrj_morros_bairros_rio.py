# This script generates a gpx file for the hills and
# districts of the city of Rio de Janeiro using the
# MongoDB collections PCRJ_Locais and PCRJ_Bairros.
import bson
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Hills and districts of the city of Rio de Janeiro", \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# For all hills in Rio de Janeiro
regx = bson.regex.Regex("^Morro")
for hill in db.PCRJ_Locais.find( { 'properties.Nome' : regx } ):
    name = hill['properties']['Nome']
    desc = hill['properties']['Descricao']
    lat = hill['geometry']['coordinates'][1]
    lon = hill['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

#  Perimeters of districts
for dist in db.PCRJ_Bairros.find():
    gpx.trkbeg(dist['properties']['NOME'])
    for geo in dist['geometry']['coordinates']:
        if dist['geometry']['type'] == 'Polygon':
            gpx.trksegbeg()
            for point in geo:
                gpx.trkpt(point[1], point[0])
                gpx.minmaxlatlon(point[1], point[0])
                
            gpx.trksegend()
        else:
            for coord in geo:
                gpx.trksegbeg()
                for point in coord:
                    gpx.trkpt(point[1], point[0])
                    gpx.minmaxlatlon(point[1], point[0])

                gpx.trksegend()

    gpx.trkend()

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()