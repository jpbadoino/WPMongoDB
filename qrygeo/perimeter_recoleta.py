# This script genaretes a gpx file for the perimeter of Recoleta
from gpx import GPX
from pymongo import MongoClient
import numpy as np

# Connect to reficio database on MongoDB server
def Database():
    server = 'localhost'
    port = 27017
    conn = MongoClient(server, port)
    return conn.reficio

db = Database()

# GPX class
gpx = GPX()
gpx.gpxbeg("Perimeter of Recoleta", \
           "Buenos Aires Data")

# Perimeter of Recoleta
gpx.trkbeg("Perimeter of Recoleta")
gpx.trksegbeg()
barrios_porteños = db.barrios_porteños
barrio =  barrios_porteños.find_one( { 'properties.BARRIO' : 'RECOLETA' } )
geometry = barrio['geometry']['coordinates']

for geo in geometry[0]:
    gpx.trkpt(geo[1],geo[0])
    gpx.minmaxlatlon(geo[1],geo[0])

gpx.trksegend()
gpx.trkend()
gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()