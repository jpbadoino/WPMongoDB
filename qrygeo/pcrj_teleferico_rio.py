# This script generates a gpx file for cable car
# stations in Rio de Janeiro City using MongoDB
# collection PCRJ_Estações_Teleférico.
from gpx import GPX
from pymongo import MongoClient

# GPX class

gpx = GPX()
gpx.gpxbeg("Cable car stations - Rio de Janeiro", \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# For all cable car stations in Rio de Janeiro
for station in db.PCRJ_Estações_Teleférico.find():
    name = station['properties']['Nome']
    print(name + ",")
    desc = "Estação do Teleférico - Cable Car Station"
    lat = station['geometry']['coordinates'][1]
    lon = station['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()