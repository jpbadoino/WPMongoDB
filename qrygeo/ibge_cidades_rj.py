# This script genaretes a gpx file for the Municipalities of
# State of Rio de Janeiro using the ibgelocalidades collection.
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Cidades do Estado do Rio de Janeiro - Cities of the State of Rio de Janeiro", \
           "IBGE (Instituto Brasileiro de Geografia e Estatística / Brazilian Institute of Geography and Statistics)")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# For cities in Rio de Janeiro State
for city in db.ibgelocalidades.find({ "$and": \
[{'properties.NM_UF' : 'RIO DE JANEIRO'}, \
{'properties.NM_CATEGOR' : 'CIDADE'}]}):
    name = city['properties']['NM_MUNICIP']
    desc = city['properties']['NM_MICRO'] + " - " + city['properties']['NM_MESO']
    lat  = city['geometry']['coordinates'][1]
    lon  = city['geometry']['coordinates'][0]
    print(name + " - " + desc)
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()