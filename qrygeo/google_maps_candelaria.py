# Google Maps Platform - Web Services - Geocoding API
# https://developers.google.com/maps/documentation/geocoding/start?hl=en
#
# This Python script shows how to geolocalize the address
# Igreja de Nossa Senhora da Candelária - Praça Pio X - Centro,
# Rio de Janeiro - RJ (Church of Our Lady of Candelaria)
# accessing Google Maps Geocoding API through an HTTP interface,
# and creates a GPX (GPS Exchange Format) file to display the
# address on the map using the OpenStreetMap.
#
# -*- coding: utf-8 -*-
from gpx import GPX
import http.client, urllib.parse
import json

# Read the Google Maps Key from 'GoogleMapsKey.txt' file
GoogleMapsKey = open('GoogleMapsKey.txt', 'r').read().rstrip("\n")

host = 'maps.googleapis.com'
path = '/maps/api/geocode/json'
address = 'Igreja de Nossa Senhora da Candelária - Praça Pio X'

params = '?address=' + urllib.parse.quote (address) + \
         '&key=' + GoogleMapsKey

def get_response():
    conn = http.client.HTTPSConnection(host)
    conn.request("GET", path + params)
    response = conn.getresponse()
    return response.read()

results = json.loads(get_response())
print(json.dumps(results, indent=4))
formatted_address = results['results'][0]['formatted_address']
lat = results['results'][0]['geometry']['location']['lat']
lon = results['results'][0]['geometry']['location']['lng']

# GPX class
gpx = GPX()
gpx.gpxbeg(address, formatted_address)
gpx.wpt(address, formatted_address, lat, lon)
gpx.minmaxlatlon(lat, lon)
gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()