# This script genaretes a gpx file for idee_faros_espana collection.
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Faros de España - Lighthouses of Spain", \
           "Geoportal IDEE - Infraestructura de Datos Espaciales de España")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# For all Spanish headlights
for faro in db.idee_faros_espana.find():
    print(faro)
    identificador_geografico = faro['properties']['identificador_geografico']
    id  = faro['properties']['id']
    lat = faro['geometry']['coordinates'][1]
    lon = faro['geometry']['coordinates'][0]
    gpx.wpt(identificador_geografico, id, lat, lon)
    gpx.minmaxlatlon(lat, lon)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()