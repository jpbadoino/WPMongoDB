# This script generates a gpx file for the
# Corcovado Railroad route in the city of
# Rio de Janeiro using the MongoDB collections
# PCRJ_Trajeto_Bonde and PCRJ_Estações_Bonde
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Corcovado Railroad in the City of Rio de Janeiro", \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# Corcovado Railroad stations
for station in db.PCRJ_Estações_Bonde.find({'properties.Trecho' : "Estrada de Ferro do Corcovado"}):
    name = station['properties']['Nome']
    desc = station['properties']['Trecho']
    lat = station['geometry']['coordinates'][1]
    lon = station['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

# Corcovado Railroad route
for route in db.PCRJ_Trajeto_Bonde.find({'properties.Nome' : "Estrada de Ferro do Corcovado"}):
    gpx.trkbeg(route['properties']['Nome'])
    if route['geometry']['type'] == 'LineString':
        gpx.trksegbeg()
        for point in route['geometry']['coordinates']:
            gpx.trkpt(point[1], point[0])
            gpx.minmaxlatlon(point[1], point[0])

        gpx.trksegend()
    else:
        for seg in route['geometry']['coordinates']:
            gpx.trksegbeg()
            for point in seg:
                gpx.trkpt(point[1], point[0])
                gpx.minmaxlatlon(point[1], point[0])

            gpx.trksegend()

    gpx.trkend()

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()