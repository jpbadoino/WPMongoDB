# This script generates a gpx file for the
# perimeter of a district of the city of
# Rio de Janeiro provided as a parameter,
# using MongoDB's RJ_Bairros collection.
import sys
from gpx import GPX
from pymongo import MongoClient

# District name as parameter
if len(sys.argv) < 2:
    print("District name parameter is required.")
    print("Syntax: " + sys.argv[0] + " district_name")
    sys.exit()

district = str(sys.argv[1])
outfile = district.replace(" ", "_").lower() + ".gpx"

# GPX class
gpx = GPX()
gpx.gpxbeg(district, \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# District perimeter
for dist in db.PCRJ_Bairros.find({'properties.NOME' : district}):
    gpx.trkbeg(dist['properties']['NOME'])
    for geo in dist['geometry']['coordinates']:
        if dist['geometry']['type'] == 'Polygon':
            gpx.trksegbeg()
            for point in geo:
                gpx.trkpt(point[1], point[0])
                gpx.minmaxlatlon(point[1], point[0])
                
            gpx.trksegend()
        else:
            for coord in geo:
                gpx.trksegbeg()
                for point in coord:
                    gpx.trkpt(point[1], point[0])
                    gpx.minmaxlatlon(point[1], point[0])

                gpx.trksegend()

    gpx.trkend()

gpx.gpxend()
gpx.write(outfile)

#  WordPress OpenStreetMap Plugin
gpx.wposmap(outfile)