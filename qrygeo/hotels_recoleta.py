# This script combines two collections to display the
# hotels in Buenos Aires in the neighborhood of Recoleta
# and the perimeter of Recoleta.
from gpx import GPX
from pymongo import MongoClient
import numpy as np

# Connect to reficio database on MongoDB server
def Database():
    server = 'localhost'
    port = 27017
    conn = MongoClient(server, port)
    return conn.reficio

db = Database()

# GPX class
gpx = GPX()
gpx.gpxbeg("Hotels in Buenos Aires in the neighborhood of Recoleta", \
           "Buenos Aires Data + Geonames")

# Geometry of the neighborhood of Recoleta
# from the barrios_porteños collection
barrios_porteños = db.barrios_porteños
barrio =  barrios_porteños.find_one( { 'properties.BARRIO' : 'RECOLETA' } )
geometry = barrio['geometry']['coordinates']

# Hotels in Recoleta, Buenos Aires
geonamesar = db.geonamesar
hotels = []
for geoname in geonamesar.find( \
{ "$and": [ \
{ "features.geometry.coordinates": \
{ "$geoWithin": \
{ "$geometry":  \
{ "type" : "Polygon" , "coordinates": geometry } \
}  \
}  \
}, \
{ "features.properties.feature_code" : "HTL" } \
]  \
} ):
    geonameid = geoname['features'][0]['properties']['geonameid']
    name = geoname['features'][0]['properties']['name']
    lat =  geoname['features'][0]['geometry']['coordinates'][1]
    lon =  geoname['features'][0]['geometry']['coordinates'][0]
    hotels.append(np.array(name))
    gpx.wpt(name, geonameid, lat, lon)
    gpx.minmaxlatlon(lat, lon)

# Perimeter of Recoleta
gpx.trkbeg("Perimeter of Recoleta")
gpx.trksegbeg()

for geo in geometry[0]:
    gpx.trkpt(geo[1],geo[0])
    gpx.minmaxlatlon(geo[1],geo[0])

gpx.trksegend()
gpx.trkend()
gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()
# Print the hotels in alphabetical order
for hotel in sorted(hotels):
    print(hotel)
