# This script converts a Tab Separated Value (TSV) file,
# with a single line containing the tab-separated
# name, description, latitude, and longitude
# fields, in a GPX (GPS eXchange Format) file.
#
# If there are multiple lines in the input file (header, etc.),
# only the last one will be used to generate the GPX file, but
# there must be at least four fields on all lines.
#
# -*- coding: utf-8 -*-
from gpx import GPX
import sys
import csv

# Input and output files
if len(sys.argv) < 2:
    print("Input file parameter is required.")
    print("Syntax: " + sys.argv[0] + " input_file")
    sys.exit()

infile = str(sys.argv[1])
filename, extension = infile.split('.')
outfile = filename + ".gpx"

# Read the TSV file
with open(infile, 'r') as tsvfile:
    fileread = csv.reader(tsvfile, delimiter='\t', quotechar='§')
    for tsv in fileread:
        if len(tsv) < 4:
            print("There should be four fields in each line:")
            print("name, description, latitude and longitude.")
            sys.exit()
        name = tsv[0]
        desc = tsv[1]
        lat  = float(tsv[2])
        lon  = float(tsv[3])

# GPX class
gpx = GPX()
gpx.gpxbeg(name, desc)
gpx.wpt(name, desc, lat, lon)
gpx.minmaxlatlon(lat, lon)
gpx.gpxend()
gpx.write(outfile)

#  WordPress OpenStreetMap Plugin
gpx.wposmap(outfile)