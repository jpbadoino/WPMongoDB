# This script generates a GPX file containing the
# latitude and longitude of the cemeteries of the
# City of Rio de Janeiro using GeoNames data
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Cemeteries of the City of Rio de Janeiro", \
           "GeoNames geographical database")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# Cemiteries in Rio
for feature in db.geonamesbr.find({ "$and" :       \
[ { 'features.properties.feature_code' : 'CMTY' }, \
  { 'features.properties.admin1_code' : '21'}      \
] } ):
    name = feature['features'][0]['properties']['name']
    desc = "GeoNames - id: " + feature['features'][0]['properties']['geonameid']
    lat =  feature['features'][0]['geometry']['coordinates'][1]
    lon =  feature['features'][0]['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)
    print(name)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()