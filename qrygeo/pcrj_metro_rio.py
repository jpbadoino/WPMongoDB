# This script generates a gpx file for the Metro (subway)
# of the city of Rio de Janeiro using the MongoDB collections
# PCRJ_Estações_Metrô and PCRJ_Trajetos_Metrô
from gpx import GPX
from pymongo import MongoClient

# GPX class
gpx = GPX()
gpx.gpxbeg("Metro (subway) of the city of Rio de Janeiro", \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# Subway stations
for station in db.PCRJ_Estações_Metrô.find():
    name = station['properties']['Nome']
    desc = station['properties']['Data_Inauguracao']
    lat = station['geometry']['coordinates'][1]
    lon = station['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

# Subway route
for route in db.PCRJ_Trajetos_Metrô.find():
    gpx.trkbeg(route['properties']['Tipo'])
    if route['geometry']['type'] == 'LineString':
        gpx.trksegbeg()
        for point in route['geometry']['coordinates']:
            gpx.trkpt(point[1], point[0])
            gpx.minmaxlatlon(point[1], point[0])

        gpx.trksegend()
    else:
        for seg in route['geometry']['coordinates']:
            gpx.trksegbeg()
            for point in seg:
                gpx.trkpt(point[1], point[0])
                gpx.minmaxlatlon(point[1], point[0])

            gpx.trksegend()

    gpx.trkend()

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()