# This script generates a gpx file for SuperVia train
# stations (Rio de Janeiro) using MongoDB collection
# PCRJ_Estações_Trem.
from gpx import GPX
from pymongo import MongoClient

# GPX class

gpx = GPX()
gpx.gpxbeg("SuperVia train stations - Rio de Janeiro", \
           "Portal de Dados Geográficos Abertos da Cidade do Rio de Janeiro")

# Connect to reficio database on MongoDB server
conn = MongoClient('localhost', 27017)
db = conn.reficio

# For all train stations in Rio de Janeiro
for station in db.PCRJ_Estações_Trem.find():
    name = station['properties']['Nome']
    print(name + ",")
    desc = "Estação da SuperVia (train station)"
    lat = station['geometry']['coordinates'][1]
    lon = station['geometry']['coordinates'][0]
    gpx.wpt(name, desc, lat, lon)
    gpx.minmaxlatlon(lat, lon)

gpx.gpxend()
gpx.write()

#  WordPress OpenStreetMap Plugin
gpx.wposmap()