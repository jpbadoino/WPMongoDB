#!/bin/bash
# Display images GPS tags values using ExitTool
exiftool -filename \
         -gpslatitude \
         -gpslongitude \
         -T \
         -n \
         -ext jpg -ext JPG *