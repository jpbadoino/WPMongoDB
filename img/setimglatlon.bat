@ECHO off
REM Set image GPS tags values using ExifTool.
REM Parameters:
REM     1 : Image file name with extension
REM     2 : GPSLatitude
REM     3 : GPSLatitudeRef (N,S)
REM     4 : GPSLongitude
REM     5 : GPSLongitudeRef (E,W)
REM Example:
REM     .\setimglatlon.bat IMG_0001.JPG 22.9102392978667 S 43.1783745848056 W
REM Path to the ExifTool executable
SET PATH=%PATH%;C:\Imagens\
REM Assign values to GPS tags
exiftool.exe -v2 ^
             -GPSMapDatum="WGS-84"                   ^
             -gps:GPSLatitude=%2                     ^
             -gps:GPSLatitudeRef=%3                  ^
             -gps:GPSLongitude=%4                    ^
             -gps:GPSLongitudeRef=%5                 ^
             -gps:GPSMeasureMode=2                   ^
             -Copyright="Halley Pacheco de Oliveira" ^
             -overwrite_original                     ^
             %1
REM Display GPS tags values
exiftool.exe -filename     ^
             -gpslatitude  ^
             -gpslongitude ^
             -T            ^
             -n            ^
             %1
