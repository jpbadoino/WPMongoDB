#!/bin/bash
# Set image GPS tags values using ExifTool.
# Parameters:
#     1 : Image file name with extension
#     2 : GPSLatitude
#     3 : GPSLatitudeRef (N,S)
#     4 : GPSLongitude
#     5 : GPSLongitudeRef (E,W)
# Example:
#     ./setimglatlon.sh IMG_0001.JPG 22.9102392978667 S 43.1783745848056 W
# Assign values to GPS tags
exiftool -v2 \
         -GPSMapDatum="WGS-84"                   \
         -gps:GPSLatitude=$2                     \
         -gps:GPSLatitudeRef=$3                  \
         -gps:GPSLongitude=$4                    \
         -gps:GPSLongitudeRef=$5                 \
         -gps:GPSMeasuremode=2                   \
         -Copyright="Halley Pacheco de Oliveira" \
         -overwrite_original                     \
         $1
# Display GPS tags values
exiftool -filename       \
         -gpslatitude    \
         -gpslongitude   \
         -T              \
         -n              \
         $1
