#!/bin/bash
# ExifTool:
#     Geotag all images in the directory from information in the GPS track
# Parameter:
#     GPS track file name with extension
# Example:
#     ./setimggpstrk.sh ./20180610_centro_santa_teresa_bondinho.gpx
# Assign values to GPS tags
exiftool -geotag=$1                              \
         -geosync="-0:00"                        \
         -overwrite_original                     \
         -Copyright="Halley Pacheco de Oliveira" \
         *.JPG
