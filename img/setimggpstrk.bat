@ECHO off
REM ExifTool
REM     Geotag all images in the directory from information in the GPS track
REM Parameters:
REM     1 : GPS track file name with extension
REM Example:
REM     .\setimggpstrk.bat .\20180610_centro_santa_teresa_bondinho.gpx
REM Path to the ExifTool executable
SET PATH=%PATH%;C:\Imagens\
REM Assign values to GPS tags
exiftool.exe -geotag=%1                              ^
             -geosync="-0:00"                        ^
             -overwrite_original                     ^
             -Copyright="Halley Pacheco de Oliveira" ^
             *.JPG
