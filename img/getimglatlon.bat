@ECHO off
REM Display images GPS tags values using ExitTool
REM Path to the ExifTool executable
SET PATH=%PATH%;C:\Imagens\
exiftool.exe -filename     ^
             -gpslatitude  ^
             -gpslongitude ^
             -T            ^
             -n            ^
             *.JPG
pause
