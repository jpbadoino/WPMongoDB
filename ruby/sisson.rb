# Load the MongoDB library
require 'mongo'
# Adjust the logger level
Mongo::Logger.logger.level = ::Logger::FATAL
# Establish a connection to the database 'reficio'
conn = Mongo::Client.new([ '127.0.0.1:27017' ], \
    :database => 'reficio')
# Display the 'reficio' database collections
conn.collections.each { |coll| puts coll.name }
# Create a cursor for the query
cursor = conn[:pages].find( 
    { :title => "Sébastien Auguste Sisson" },
    { :projection => { :title => 1, :excerpt => 1 } }
)
# Display the query result
cursor.each { |doc| puts doc }
# Close the connection
conn.close
