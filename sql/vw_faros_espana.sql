DROP VIEW IF EXISTS `vw_faros_espana`;
CREATE VIEW `vw_faros_espana` AS
SELECT `identificador_geografico`,
       `id`,
       `lat_etrs89_regcan95`,
       `long_etrs89_regcan95`
FROM `NGBE`
WHERE `codigo_ngbe_text` = 'Poblaciones y construcciones.Edificación'
AND `identificador_geografico` LIKE 'Faro %'
AND NOT `lat_etrs89_regcan95` IS NULL;